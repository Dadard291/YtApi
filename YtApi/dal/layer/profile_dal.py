from api.common.common import check_password
from api.common.error_messages import *
from api.common.exceptions import DalException
from api.dal.models.profile_model import ProfileEntity


class ProfileDal:

    def __init__(self, session):
        self.session = session

    def get_query(self):
        return self.session.query(ProfileEntity)

    def create_profile(self, username, hashed_password, profile_key):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is not None:
                raise DalException(USER_ALREADY_EXISTS)

            p = ProfileEntity(username, hashed_password, profile_key)
            self.session.add(p)
            self.session.commit()
            return p.profile_key
        except Exception as e:
            raise DalException(str(e))

    def delete_profile(self, username, encoded_password):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            self.session.delete(p)
            self.session.commit()
            return p.profile_key

        except Exception as e:
            raise DalException(str(e))

    def get_profile(self, username, encoded_password):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            return p.profile_key

        except Exception as e:
            raise DalException(str(e))

    def update_profile(self, username, encoded_password, new_hashed_password):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            p.hashed_password = new_hashed_password
            self.session.commit()

            return p.profile_key

        except Exception as e:
            raise DalException(str(e))

