from datetime import datetime

from api.common.error_messages import *
from api.common.exceptions import DalException
from api.dal.models.profile_model import ProfileEntity
from api.dal.models.video_model import VideoEntity
from api.dal.repository.connector import Session
from api.dal.yt_api_call import YtApiCaller


def check_profile_key(profile_key):
    session = Session()
    p = session.query(ProfileEntity).filter_by(profile_key=profile_key).first()
    if p is None:
        raise DalException(WRONG_KEY)


class VideoDal:

    def __init__(self):
        self.caller = YtApiCaller()

    def get_query(self, session):
        return session.query(VideoEntity)

    def add_playlist(self, profile_key, playlist_id):
        try:
            session = Session()
            check_profile_key(profile_key)
            video_added = list()
            video_list = self.caller.list_video_from_playlist(playlist_id)
            for v in video_list:
                new_video = self.caller.entity_from_item(v, profile_key)
                session.add(new_video)
                video_added.append(new_video)

            # apply all additions
            session.commit()

            return video_added
        except Exception as e:
            raise DalException(str(e))

    def add_video(self, profile_key, video_id):
        session = Session()
        try:
            check_profile_key(profile_key)

            # check_duplicated = VideoEntity.query.filter_by(profile_key=profile_key, video_id=video_id).first()
            check_duplicated = self.get_query(session).filter_by(profile_key=profile_key, video_id=video_id).first()
            if check_duplicated is not None:
                raise DalException(VIDEO_DUPLICATE)

            # video_response = self.caller.fetch_video_details(video_id)
            # new_video = self.caller.entity_from_item(video_response, profile_key)
            new_video = VideoEntity(video_id=video_id,
                                    profile_key=profile_key,
                                    url="url:_{}".format(video_id),
                                    title="title_{}".format(video_id),
                                    author="author_{}".format(video_id),
                                    album="album_{}".format(video_id),
                                    date=datetime.now().date())

            session.add(new_video)
            session.commit()
            return new_video.to_dict()
        except Exception as e:
            session.rollback()
            raise DalException(str(e))
        finally:
            session.close()

    def delete_video(self, profile_key, video_id):
        try:
            session = Session()
            check_profile_key(profile_key)

            # v = VideoEntity.query.filter_by(profile_key=profile_key, video_id=video_id).first()
            v = self.get_query(session).filter_by(profile_key=profile_key, video_id=video_id).first()
            if v is None:
                raise DalException(VIDEO_NOT_EXISTS)

            session.delete(v)
            session.commit()
            return v.to_dict()

        except Exception as e:
            raise DalException(str(e))

    def list_video(self, profile_key):
        try:
            session = Session()
            check_profile_key(profile_key)

            # v_list = VideoEntity.query.filter_by(profile_key=profile_key).all()
            v_list = self.get_query(session).filter_by(profile_key=profile_key).all()

            # serialize results
            results_list = list()
            for record in v_list:
                results_list.append(record.to_dict())

            return results_list

        except Exception as e:
            raise DalException(str(e))

    def update_video(self, profile_key, video_id, kwargs):
        try:
            session = Session()
            check_profile_key(profile_key)

            # v = VideoEntity.query.filter_by(profile_key=profile_key, video_id=video_id).first()
            v = self.get_query(session).filter_by(profile_key=profile_key, video_id=video_id).first()
            if v is None:
                raise DalException(VIDEO_NOT_EXISTS)

            if 'title' in kwargs.keys() and kwargs.get('title') != '':
                v.title = kwargs.get('title')

            if 'author' in kwargs.keys() and kwargs.get('author') != '':
                v.author = kwargs.get('author')

            if 'album' in kwargs.keys() and kwargs.get('album') != '':
                v.album = kwargs.get('album')

            if 'date' in kwargs.keys() and kwargs.get('date') != '':
                raw_date = kwargs.get('date')
                v.date = datetime.strptime(raw_date, '%Y-%m-%d').date()  # ex : 2019-07-02

            session.commit()
            return v.to_dict()

        except Exception as e:
            raise DalException(str(e))
