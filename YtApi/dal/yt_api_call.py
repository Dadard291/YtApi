from datetime import datetime

import requests
import json

from requests.auth import HTTPProxyAuth

from api.common.config_accessor import ConfigAccessor
from api.dal.models.video_model import VideoEntity


class YtApiCaller:

    def __init__(self):
        self.config_dict = ConfigAccessor.config_access('yt_api')
        self.api_key = self.config_dict['api_key']
        self.video_info_url = self.config_dict['video_info_url']
        self.playlist_info_url = self.config_dict['playlist_info_url']
        self.url_format = self.config_dict['url_format']
        self.next_page_token = self.config_dict['next_page_token']
        self.playlist_info_url_next = self.config_dict['playlist_info_url_next']
        self.proxy = {
            'http': self.config_dict['proxy_url'],
            'https': self.config_dict['proxy_url']
        }

        self.auth = HTTPProxyAuth('fcharpen090318', '#Dadard_29=m9')

        self.playlist_params = self.config_dict['playlist_params']
        self.video_params = self.config_dict['video_params']

    def list_video_from_playlist(self, playlist_id):
        video_list = list()
        params = self.playlist_params.update({
            'playlistId': playlist_id,
            'key': self.api_key
        })
        resp = requests.get(self.playlist_info_url, params=params, proxies=self.proxy)
        resp_dict = json.loads(resp)
        video_list.extend(resp_dict['items'])
        while self.next_page_token in resp_dict.keys():
            next_page_token_value = resp_dict[self.next_page_token]
            params['nextPageToken'] = next_page_token_value
            resp = requests.get(self.playlist_info_url_next, params=params, proxies=self.proxy)
            resp_dict = json.loads(resp)
            video_list.extend(resp_dict['items'])

        return video_list

    def fetch_video_details(self, video_id):
        params = self.video_params.update({
            'id': video_id,
            'key': self.api_key
        })
        resp_dict = json.loads(requests.get(self.video_info_url, params=params, proxies=self.proxy, auth=self.auth))
        return resp_dict['items'][0]

    def entity_from_item(self, video_dict, profile_key):
        video_id = video_dict['snippet']['resourceId']['videoId']
        title = video_dict['snippet']['title']
        author = video_dict['snippet']['channelTitle']
        album = author
        date = datetime.strptime(video_dict['snippet']['publishedAt'], '%Y-%m-%dT%H:%M:%S.%fZ').date()

        url = self.url_format.format(video_id)

        return VideoEntity(video_id, profile_key, url, title, author, album, date)






