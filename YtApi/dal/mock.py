from api.dal.models.video_model import VideoEntity

mockApiKey = 'apikey'
mockVideoList = [
    VideoEntity('U6fMXohin8M1', 'https://youtu.be/U6fMXohin8M1', 'title1', 'author1', 'album1', '01/09/2019').__dict__,
    VideoEntity('U6fMXohin8M2', 'https://youtu.be/U6fMXohin8M2', 'title2', 'author2', 'album2', '02/09/2019').__dict__,
    VideoEntity('U6fMXohin8M3', 'https://youtu.be/U6fMXohin8M3', 'title3', 'author3', 'album3', '03/09/2019').__dict__,
    VideoEntity('U6fMXohin8M4', 'https://youtu.be/U6fMXohin8M4', 'title4', 'author4', 'album4', '04/09/2019').__dict__,
    VideoEntity('U6fMXohin8M5', 'https://youtu.be/U6fMXohin8M5', 'title5', 'author5', 'album5', '05/09/2019').__dict__
]
