from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from api.dal.repository.connector import Base


class ProfileEntity(Base):
    __tablename__ = 'profile'
    profile_key = Column(String(70), unique=True, primary_key=True, default=1)
    username = Column(String(70), unique=True)
    hashed_password = Column(String(70))
    videos = relationship('VideoEntity', backref=__tablename__, lazy=True)

    def __init__(self, username=None, hashed_password=None, profile_key=None):
        self.username = username
        self.hashed_password = hashed_password
        self.profile_key = profile_key

    def __repr__(self):
        return '<User %r>' % self.username
