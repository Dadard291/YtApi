from api.dal.repository.connector import Base
from sqlalchemy import Column, String, ForeignKey, Date


class VideoEntity(Base):
    __tablename__ = 'video'
    video_id = Column(String(30), primary_key=True)
    profile_key = Column(String(70), ForeignKey('profile.profile_key'))
    url = Column(String(50), nullable=False)
    title = Column(String(30), nullable=False)
    author = Column(String(30))
    album = Column(String(30))
    date = Column(Date())

    def __init__(self, video_id=None, profile_key=None, url=None, title=None, author=None, album=None, date=None):
        self.profile_key = profile_key
        self.video_id = video_id
        self.url = url
        self.title = title
        self.author = author
        self.album = album
        self.date = date

    def __repr__(self):
        return '<Video {} : {}>'.format(self.title, self.video_id)

    def to_dict(self):
        return dict(
            video_id=self.video_id,
            url=self.url,
            title=self.title,
            author=self.author,
            album=self.album,
            date=str(self.date)
        )

