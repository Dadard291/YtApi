from sys import stderr

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from api.common.config_accessor import ConfigAccessor

config_dict = ConfigAccessor.config_access("db")
host = config_dict['host']
user = config_dict['user']
password = config_dict['password']
database = config_dict['database']

engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(user, password, host, database), convert_unicode=True)
Session = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine
)

Base = declarative_base()


def init_db():
    try:
        Base.metadata.create_all(bind=engine)
    except Exception:
        # avoid MASSIVE error messages when docker compose starts, and databases not still up
        print('database down', file=stderr)
