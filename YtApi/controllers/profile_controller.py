from flask import request
from flask_restplus import Resource

from api.host.host import profile_ns
from api.common.common import format_error, format_response, check_query_parameter
from api.common.error_messages import MISSING_PARAMETER
from api.common.exceptions import BusinessException
from api.manager.profile_manager import ProfileManager


def get_credentials(login_password):
    l = login_password.split(':')
    return l[0], l[1]


@profile_ns.route('/')
class Profile(Resource):
    manager = ProfileManager()

    @profile_ns.doc(
        security="login_password"
    )
    def get(self):
        try:
            username, password = get_credentials(request.args.get('login_password'))
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)
            else:
                apiKey = self.manager.get_profile(username, password)
                return format_response('key retrieved', dict(key=apiKey))
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))

    @profile_ns.doc(
        security="login_password"
    )
    def post(self):
        try:
            username, password = get_credentials(request.args.get('login_password'))
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)

            apiKey = self.manager.create_profile(username, password)
            return format_response('account created', dict(key=apiKey))

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @profile_ns.doc(
        security="login_password"
    )
    def delete(self):
        try:
            username, password = get_credentials(request.args.get('login_password'))
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)

            apiKey = self.manager.delete_profile(username, password)
            return format_response('account deleted', dict(key=apiKey))
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @profile_ns.doc(
        security="login_password",
        params={
            'new_password': 'the new password'
        }
    )
    def put(self):
        try:
            username, password = get_credentials(request.args.get('login_password'))
            new_password = request.args.get('new_password')
            if not check_query_parameter(username, password, new_password):
                return format_error(MISSING_PARAMETER)

            apiKey = self.manager.update_profile(username, password, new_password)
            return format_response('password updated', dict(key=apiKey))
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))
