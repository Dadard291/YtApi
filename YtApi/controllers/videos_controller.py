from flask import request, json
from flask_restplus import Resource, fields

from api.host.host import video_ns, api
from api.common.common import format_error, format_response, launch_job, check_query_parameter
from api.common.error_messages import MISSING_PARAMETER
from api.common.exceptions import BusinessException
from api.manager.video_manager import VideoManager

video_fields = api.model('Video', {
    'title': fields.String,
    'author': fields.String,
    'album': fields.String,
    'date': fields.Date
})

manager = VideoManager()


@video_ns.route('/playlist/<playlist_id>')
@video_ns.doc(
    description="manage playlist",
    security="api_key"
)
class VideoPlaylist(Resource):
    def post(self, playlist_id):
        try:
            key = request.args.get('key')
            if not check_query_parameter(key, playlist_id):
                return format_error(MISSING_PARAMETER)
            else:
                video_list = manager.add_playlist(key, playlist_id)
                return format_response('add playlist successful', {'count': len(video_list), 'list': video_list})
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))


@video_ns.route('/list/')
@video_ns.doc(
    description="list the video currently stored",
    security="api_key"
)
class VideoList(Resource):
    def get(self):
        try:
            key = request.args.get('key')
            if not check_query_parameter(key):
                return format_error(MISSING_PARAMETER)
            else:
                video_list = manager.list_video(key)
                return format_response('listing successful', {'count': len(video_list), 'list': video_list})
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))


@video_ns.route('/')
@video_ns.doc(
    description="manage the whole set of the videos currently stored : ",
    security="api_key"
)
class Video(Resource):
    # NEED REFACTO
    @video_ns.doc(
        description="download all"
    )
    def get(self):
        try:
            key = request.args.get('key')
            if not check_query_parameter(key):
                return format_error(MISSING_PARAMETER)
            else:
                launch_job()
                return format_response('download all launch', {})
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))


@video_ns.route('/<video_id>/')
@video_ns.doc(
    description='manage videos by id : ',
    security="api_key",
    params={
        'video_id': 'the video id'
    }
)
class VideoId(Resource):

    def post(self, video_id):
        try:
            key = request.args.get('key')
            if not check_query_parameter(key, video_id):
                return format_error(MISSING_PARAMETER)

            video = manager.add_video(key, video_id)
            return format_response('video added', dict(video=video))
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))

    def delete(self, video_id):
        try:
            key = request.args.get('key')
            if not check_query_parameter(key, video_id):
                return format_error(MISSING_PARAMETER)

            video = manager.delete_video(key, video_id)
            return format_response('video deleted', dict(video=video))
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))

    @video_ns.expect(video_fields)
    def put(self, video_id):
        try:
            key = request.args.get('key')
            if not check_query_parameter(key, video_id):
                return format_error(MISSING_PARAMETER)

            body = json.loads(request.data.decode("utf-8"))
            video = manager.update_video(key, video_id, body)
            return format_response('video updated', dict(video=video))
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))
