from flask import Blueprint
from flask_restplus import Api

# init the REST API nested into the flask application with the blueprint
api_v1 = Blueprint('api', __name__, url_prefix='/yt_api')

api = Api(
    api_v1,
    title='sample api'
)

video_authorization = {
    "api_key": {
        "type": "apiKey",
        "in": "query",
        "name": "key"
    }
}

profile_authorization = {
    "login_password": {
        "type": "apiKey",
        "in": "query",
        "name": "login_password"
    }
}

# define the namespaces
profile_ns = api.namespace('profile', description='profile management', authorizations=profile_authorization)
video_ns = api.namespace('video', description='video management', authorizations=video_authorization)
job_ns = api.namespace('job', description='job management')

# execute the controllers files, in order to register the routes to the namespaces
from api.controllers.profile_controller import *
from api.controllers.videos_controller import *

