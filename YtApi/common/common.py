from base64 import b64encode
from hashlib import sha256
from time import sleep

from bcrypt import hashpw, gensalt, checkpw


def format_error(msg):
    return dict(status='ko', message=msg, content={})


def format_response(msg, payload):
    return dict(status='ok', message=msg, content=payload)


def launch_job():
    current_job = 0
    while current_job != 100:
        current_job += 1
        sleep(2)


def check_password(encoded_password, hashed_db_password):
    return checkpw(encoded_password, hashed_db_password.encode("utf-8"))


def encode_password(password):
    return b64encode(sha256(password.encode("utf-8")).digest())


def hash_password(password):
    encoded = encode_password(password)
    hashed = hashpw(encoded, gensalt()).decode("utf-8")
    return hashed


def check_query_parameter(*args):
    for a in args:
        if a is None or a == "":
            return False

    return True
