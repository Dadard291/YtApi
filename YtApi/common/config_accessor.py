import json
import os


class ConfigAccessor:
    @classmethod
    def config_access(cls, key=None):
        try:
            base_path = 'config'
            if 'API_ENV' in os.environ:
                path = os.path.join(base_path, os.environ['API_ENV'])

                with open(os.path.join(path, 'config.json'), "r") as f:
                    content = f.read()
                    body = json.loads(content)
                    if key is None:
                        return body

                    return body[key]
            else:
                raise

        except Exception as e:
            raise Exception("error while loading configuration: {}".format(str(e)))
