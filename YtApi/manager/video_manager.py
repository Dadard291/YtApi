from api.common.exceptions import BusinessException, DalException
from api.dal.layer.video_dal import VideoDal


class VideoManager:
    def __init__(self):
        self.dal = VideoDal()

    def add_playlist(self, profile_key, playlist_id):
        try:
            return self.dal.add_playlist(profile_key, playlist_id)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def add_video(self, profile_key, video_id):
        try:
            return self.dal.add_video(profile_key, video_id)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def delete_video(self, profile_key, video_id):
        try:
            return self.dal.delete_video(profile_key, video_id)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def list_video(self, profile_key):
        try:
            return self.dal.list_video(profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def update_video(self, profile_key, video_id, kwargs):
        try:
            return self.dal.update_video(profile_key, video_id, kwargs)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))
