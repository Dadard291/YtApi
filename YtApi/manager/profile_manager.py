from random import randrange
from hashlib import sha256

from api.common.common import hash_password, encode_password
from api.common.exceptions import DalException, BusinessException
from api.dal.layer.profile_dal import ProfileDal
from api.dal.repository.connector import Session


class ProfileManager:
    def __init__(self):
        session = Session()
        self.dal = ProfileDal(session)

    def create_profile(self, username, password):
        try:
            r = randrange(1, 1000)
            profile_key = sha256(str(r).encode()).hexdigest()
            hashed_password = hash_password(password)
            return self.dal.create_profile(username, hashed_password, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def delete_profile(self, username, password):
        try:
            encoded_password = encode_password(password)
            return self.dal.delete_profile(username, encoded_password)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def get_profile(self, username, password):
        try:
            encoded_password = encode_password(password)
            return self.dal.get_profile(username, encoded_password)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def update_profile(self, username, password, new_password):
        try:
            encoded_password = encode_password(password)
            new_hashed_password = hash_password(new_password)
            return self.dal.update_profile(username, encoded_password, new_hashed_password)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))
