FROM python:latest

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

ENV API_ENV=PROD

COPY . .

USER root
RUN chmod a+w ./logs

CMD [ "python", "./app.py" ]
